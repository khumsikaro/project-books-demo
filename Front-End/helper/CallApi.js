import axios from 'axios'

import { server } from './config/config'
import { isError } from './utils/isError'

const APIKey = 'f44afe2a-a38d-49a3-a84c-30b28fc16c02'
const lang = 'TH'

axios.defaults.timeout = 90000

export const POST = (path, params, token) => {
  let url = server + path
  const _token = token ? { Authorization: `Bearer ${token}` } : null
  // console.log('================== url POST ==============', url)
  // console.log('================== params POST ==============', params)
  return new Promise((resolve, reject) => {
    axios.post(url, params, {
      headers: {
        ..._token,
        'Storefront-Key': APIKey,
        'Content-Type': 'application/json',
        'Language': lang
      }
    }).then(
      response => {
        let result = isError(response)
        if (result === false) {
          let res = { success: false, response: response.data.response, errorCode: response.status }
          resolve(res)
        } else {
          let res = { success: true, response: response.data.response, errorCode: response.status }
          resolve(res)
        }
      }
    ).catch(error => {
      // console.log('================== ERROR ==============', error.response)
      if (error.response) {
        if (error.response.status === 401) {
          if (error.response.statusText === 'Unauthorized') {
            let rej = {
              success: false,
              response: {
                detail: 'Unauthorized'
              },
              errorCode: error.response.status
            }
            reject(rej)
          } else {
            let rej = {
              success: false,
              response: error.response.data,
              errorCode: error.response.status
            }
            reject(rej)
          }
        } else {
          let rej = {
            success: false,
            response: error.response.data,
            errorCode: error.response.status
          }
          reject(rej)
        }
      }
    })
  })
}

export const GET = (path, token) => {
  let url = server + path
  const _token = token ? { Authorization: `Bearer ${token}` } : null
  return new Promise((resolve, reject) => {
    axios.get(url, {
      headers: {
        ..._token,
        'Storefront-Key': APIKey,
        'Content-Type': 'application/json',
        'Language': lang
      }
    }).then(
      response => {
        let result = isError(response)
        if (result === false) {
          let res = { success: false, response: response.data.response, errorCode: response.status }
          resolve(res)
        } else {
          let res = { success: true, response: response.data.response, errorCode: response.status }
          resolve(res)
        }
      }
    ).catch(error => {
      // console.log('================== ERROR ==============', error.response)
      if (error.response) {
        if (error.response.status === 401) {
          if (error.response.statusText === 'Unauthorized') {
            let rej = {
              success: false,
              response: {
                detail: 'Unauthorized'
              },
              errorCode: error.response.status
            }
            reject(rej)
          } else {
            let rej = {
              success: false,
              response: error.response.data.response,
              errorCode: error.response.status
            }
            reject(rej)
          }
        } else {
          let rej = {
            success: false,
            response: error.response.data.response,
            errorCode: error.response.status
          }
          reject(rej)
        }
      }
    })
  })
}

export const PUT = (path, params, token) => {
  let url = server + path
  const _token = token ? { Authorization: `Bearer ${token}` } : null

  return new Promise((resolve, reject) => {
    axios.put(url, params, {
      headers: {
        ..._token,
        'Storefront-Key': APIKey,
        'Content-Type': 'application/json',
        'Language': lang
      }
    }).then(
      response => {
        let result = isError(response)
        if (result === false) {
          let res = { success: false, response: response.data.response, errorCode: response.status }
          resolve(res)
        } else {
          let res = { success: true, response: response.data.response, errorCode: response.status }
          resolve(res)
        }
      }
    ).catch(error => {
      if (error.response) {
        if (error.response.status === 401) {
          if (error.response.statusText === 'Unauthorized') {
            let rej = {
              success: false,
              response: {
                detail: 'Unauthorized'
              },
              errorCode: error.response.status
            }
            reject(rej)
          } else {
            let rej = {
              success: false,
              response: error.response.data,
              errorCode: error.response.status
            }
            reject(rej)
          }
        } else {
          let rej = {
            success: false,
            response: error.response.data,
            errorCode: error.response.status
          }
          reject(rej)
        }
      }
    })
  })
}

export const DELETE = (path, token) => {
  let url = server + path
  const _token = token ? { Authorization: `Bearer ${token}` } : null
  return new Promise((resolve, reject) => {
    axios.delete(url, {
      headers: {
        ..._token,
        'Storefront-Key': APIKey,
        'Content-Type': 'application/json',
        'Language': lang
      }
    }).then(
      response => {
        let result = isError(response)
        if (result === false) {
          let res = { success: false, response: response.data.response, errorCode: response.status }
          resolve(res)
        } else {
          let res = { success: true, response: response.data.response, errorCode: response.status }
          resolve(res)
        }
      }
    ).catch(error => {
      if (error.response) {
        if (error.response.status === 401) {
          if (error.response.statusText === 'Unauthorized') {
            let rej = {
              success: false,
              response: {
                detail: 'Unauthorized'
              },
              errorCode: error.response.status
            }
            reject(rej)
          } else {
            let rej = {
              success: false,
              response: error.response.data,
              errorCode: error.response.status
            }
            reject(rej)
          }
        } else {
          let rej = {
            success: false,
            response: error.response.data,
            errorCode: error.response.status
          }
          reject(rej)
        }
      }
    })
  })
}
