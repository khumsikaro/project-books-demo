import ModalBasic from './ModalBasic'
import ModalQR from './ModalQR'
import ModalAddAddress from './ModalAddAddress'
import ModalChangePassword from './ModalChangePassword'

export { ModalBasic , ModalQR, ModalAddAddress, ModalChangePassword } 