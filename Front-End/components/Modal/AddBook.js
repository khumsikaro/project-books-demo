import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Form, FormGroup, Label, Input, FormText } from "reactstrap";
import axios from 'axios'

export default function AddBook(props) {
const Url ="192.168.31.00"

  function Senddata(data) {
    console.log(data);
    axios.post(Url+"AddBook",data).then(res=>{
      console.log(res.data);
      props.toggle
  })
  }


  const [accountList, setAccountList] = useState([]);
  const [account, setaccount] = useState({
    imgbook: "",
    name: "",
    number: "",
    detail: "",
  });
  const { buttonLabel, className } = props;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  return (
    <div>
      <Modal isOpen={props.isModal} toggle={props.toggle} className={className}>
        <ModalHeader toggle={props.toggle}>เพิ่มหนังสือ</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label>ภาพหนังสือ</Label>
              <Input
                type="file"
                name="imgbook"
                id="imgbook"
                value={account.imgbook}
                onChange={e => {
                  setaccount({ ...account, imgbook: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>ชื่อหนังสือ</Label>
              <Input
                type="text"
                name="name"
                id="name"
                placeholder="name"
                value={account.name}
                onChange={e => {
                  setaccount({ ...account, name: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>จำนวน</Label>
              <Input
                type="number"
                min={1}
                name="number"
                id="number"
                placeholder="number"
                value={account.number}
                onChange={e => {
                  setaccount({ ...account, number: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>รายละเอียด</Label>
              <Input
                type="textarea"
                name="detail"
                id="detail"
                placeholder="detail"
                value={account.detail}
                onChange={e => {
                  setaccount({ ...account, detail: e.target.value });
                }}
              />
            </FormGroup>
            <Button
              color="primary"
              onClick={e => {
                e.preventDefault();
                setAccountList({ ...account, index: accountList.length });
                Senddata({ ...account});
              }}
            >
              ยืนยัน
            </Button>{" "}
            <Button color="secondary" onClick={props.toggle}>
              ยกเลิก
            </Button>
          </Form>
        </ModalBody>
        <ModalFooter></ModalFooter>
      </Modal>
    </div>
  );
}