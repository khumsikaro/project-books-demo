import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Form, FormGroup, Label, Input, FormText } from "reactstrap";
import axios from 'axios'

export default function DeleteBook(props) {
const Url ="192.168.31.00"

// console.log("ข้อมูลที่ส่งมา",props.data);

//   function Deletebook(data) {
//     axios.post(Url+"/DeleteBook/:"+data).then(res=>{
//       console.log(res.data);
//       props.toggle
//   })
//   }

  const { buttonLabel, className } = props;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  return (
    <div>
      <Modal isOpen={props.isModal} toggle={props.toggle} className={className}>
        <ModalHeader toggle={props.toggle}>ลบหนังสือ</ModalHeader>
        <ModalBody>
        </ModalBody>
        <p>
            คุณต้องการลบ {props.data} หรือไม่ ?
        </p>
        <ModalFooter>
        <Button color="danger" >ลบ</Button>{' '}
          <Button color="secondary" onClick={props.toggle}>ยกเลิก</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}