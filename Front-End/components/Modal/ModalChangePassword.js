import React, { Component } from 'react'
import { TiWarningOutline, TiTick } from 'react-icons/ti'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

export default class ModalChangePassword extends Component {

  render() {
    return (
      <Modal isOpen={this.props.modal} toggle={this.props.toggle} className='modal-Add-Address'>
        <ModalBody>
          <div className='row'>
            <div className='col-10 text-left'>
              <span className='title-modal'> Change Password </span>
            </div>
            <div className='col-2 middle-box'>
              <span className='text-close' onClick={this.props.toggle}>X</span>
            </div>
            <div className='col-12 mt-4 text-tag-html text-muted'>
              {`
              To Protect your Account, Make sure your Password is : 
              - Longer than 7 characters 
              - Password must be different from your previous password.
              `}
            </div>

            <div className='col-12 mt-5'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='currentPassword'>Current Password</label>
                <input type='text' className='form-control' id='currentPassword' placeholder='' />
              </div>
            </div>
            <div className='col-12 mt-3'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='newPassword'>New Password</label>
                <input type='text' className='form-control' id='newPassword' placeholder='' />
              </div>
            </div>
            <div className='col-12 mt-3'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='confirmNewPassword'>Confirm New Password</label>
                <input type='text' className='form-control' id='confirmNewPassword' placeholder='' />
              </div>
            </div>

            <div className='col-12 mt-5 text-center'>
              <button type='button' className='btn-citrus btn-block btn-change'>Change Password</button>
            </div>
          </div>
        </ModalBody>
      </Modal>
    )
  }
}
