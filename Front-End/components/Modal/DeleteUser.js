import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Form, FormGroup, Label, Input, FormText } from "reactstrap";
import axios from 'axios'

export default function DeleteUser(props) {
  
  const Url ="192.168.31.00"
  function DeleteUser(data) {
    axios.post(Url+"/DeleteUser/:"+data).then(res=>{
      console.log(res.data);
      props.toggle
  })
  }
  const { buttonLabel, className } = props;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  return (
    <div>
      <Modal isOpen={props.isModal} toggle={props.toggle} className={className}>
        <ModalHeader toggle={props.toggle}>ลบสมาชิก</ModalHeader>
        <ModalBody>
        </ModalBody>
        <p>&nbsp;

            คุณต้องการลบ {props.data} หรือไม่ ?
        </p>
        <ModalFooter>
        <Button color="danger" onClick={DeleteUser(props.data)}>ลบ</Button>{' '}
          <Button color="secondary" onClick={props.toggle}>ยกเลิก</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}