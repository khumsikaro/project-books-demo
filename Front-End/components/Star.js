import React, { Component } from 'react'
import times from 'lodash/times'
import { FaStar } from 'react-icons/fa'


export default class Star extends Component {
    render() {
        const point = (this.props.rating || 0).toFixed(0)
        const inActive = 5 - point
        return (
            <div>
                {
                    times(point, n => <FaStar key={n} className={`star-active ${this.props.className}`} />)
                }
                {
                    times(inActive, n => <FaStar key={n} className={`star-unactive ${this.props.className}`} />)
                }
            </div>
        )
    }
}
