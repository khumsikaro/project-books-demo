import React, { Component } from 'react'
import NavLink from './NavLink'

export default class Sidebar extends Component {
    render() {
        return (
            <div className="container-fluid bg-white pt-2">
                <div className="row">
                    <div className="container">
                        <nav className="burger-nav navbar navbar-expand-lg navbar-light w-100">
                            {/* <!-- Collect the nav links, forms, and other content for toggling --> */}
                            <div className={`navbar-collapse offset w-100 ${this.props.toggleSidebar === true && 'active'}`}>
                                <div className="row w-100 mr-0">
                                    <div className="col-lg-12 pr-0">
                                        <ul className="nav navbar-nav center_nav pull-right">
                                            <li className="nav-item">
                                                <NavLink activeClassName='active' href='/'>
                                                    <a className='nav-link home-link'>Home</a>
                                                </NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink activeClassName='active' href='/#sector-about'>
                                                    <a className='nav-link home-link' href='#sector-about'>ABOUT US</a>
                                                </NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink activeClassName='active' href='/#sector-recommended'>
                                                    <a className='nav-link home-link' href='#sector-recommended'>PRODUCTS</a>
                                                </NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink activeClassName='active' href='/#sector-repurchase'>
                                                    <a className='nav-link home-link' href='#sector-repurchase'>PROMOTION</a>
                                                </NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink activeClassName='active' href='/#sector-travel'>
                                                    <a className='nav-link home-link' href='#sector-travel'>TRAVEL</a>
                                                </NavLink>
                                            </li>
                                            <li className="nav-item">
                                                <NavLink activeClassName='active' href='/contact'>
                                                    <a className='nav-link home-link'>CONTACT</a>
                                                </NavLink>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        )
    }
}
