import React, { Component } from 'react'
import { connect } from 'react-redux'
import Head from 'next/head'

import Header from './Header'
import Footer from './Footer'
import { sidebarOn, sidebarOff } from '../actions/sidebar'

export class layout extends Component {

  switchSidebar = (value) => {
    if (value === true) {
      this.props.sidebarOff()
    } else {
      this.props.sidebarOn()
    }
  }

  render() {
    const { toggleSidebar } = this.props.sidebar
    return (
      <React.Fragment>
        <Head>
          <meta charSet='UTF-8' />
          <meta name='viewport' content='width=device-width, initial-scale=1' />
        </Head>
        <Header toggleSidebar={toggleSidebar} switchSidebar={(value) => this.switchSidebar(value)} account={this.props.account}/>
        <div className='wrapper'>
          <div className='content-page'>
            {this.props.children}
          </div>
          {/* <div className={`overlay ${toggleSidebar === true && 'active'}`} onClick={() => this.props.sidebarOff()}></div> */}
        </div>
        <Footer />
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  sidebar: state.sidebar,
  account: state.account
})

const mapDispatchToProps = {
  sidebarOff,
  sidebarOn
}

export default connect(mapStateToProps, mapDispatchToProps)(layout)
