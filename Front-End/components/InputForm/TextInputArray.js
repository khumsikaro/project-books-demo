import React, { Component } from 'react'
import { Field } from 'redux-form'
import classNames from 'classnames'
import TextInput from './TextInput'
import { FiMinusCircle } from 'react-icons/fi'

export default class TextInputArray extends Component {
    render() {

        const { fields, meta: { touched, error, submitFailed } } = this.props

        const inputClass = classNames('col-12 text-left box-sub-option', {
            ' is-invalid ': (touched || submitFailed) && error
        })

        return (
            <React.Fragment>
                <div className='form-group'>
                    <div className={inputClass}>
                        <button type='button' className='btn-outline-citrus my-4' onClick={() => fields.push('')}>
                            <span className='span-add-option'>
                                Add Option
                                <span className='text-danger'>*</span>
                            </span>
                        </button>

                        {(touched || submitFailed) && error && <span className='ml-3 text-danger'>{error}</span>}
                        {fields.map((subArray, index) => (
                            <div className='row' key={index}>
                                <div className='col-10 text-left'>
                                    <Field
                                        // Properties Input
                                        placeholder=''
                                        component={TextInput}
                                        className='form-control'
                                        classLabel='text-muted'
                                        label={`Option Name #${index + 1}`}
                                        type='text'
                                        name={`${subArray}`}
                                        id={`${subArray}`}
                                        required={true}
                                    />
                                </div>
                                <div className='col-2 mt-3 middle-left-box'>
                                    <FiMinusCircle className='btn-minus' onClick={() => fields.remove(index)} />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>


            </React.Fragment>
        )
    }
}
